<?xml version="1.0" encoding="utf-8"?>
<style xmlns="http://purl.org/net/xbiblio/csl" class="note" version="1.0" et-al-min="3" et-al-use-first="1" initialize-with="." demote-non-dropping-particle="sort-only" delimiter-precedes-last="never" default-locale="en-GB">
  <info>
    <title>IBFD Standard Citations and References (with page label)</title>
    <id>http://www.zotero.org/styles/ibfd-standard-citations-and-references-with-page-label</id>
    <link href="http://www.zotero.org/styles/ibfd-standard-citations-and-references-with-page-label" rel="self"/>
    <link href="http://www.zotero.org/styles/bluebook-law-review" rel="template"/>
    <link href="https://www.ibfd.org/sites/ibfd.org/files/content/pdf/Guidelines-IBFD-Standard-Citations-References.pdf" rel="documentation"/>
    <author>
      <name>Georg Mayr-Duffner</name>
      <email>georg.mayr-duffner@wu.ac.at</email>
    </author>
    <category citation-format="note"/>
    <category field="law"/>
    <summary>IBFD Standard Citations and References. Contains off-guidlines addition of page labels as used in IBFD articles.</summary>
    <updated>2020-08-07T13:51:31+02:00</updated>
    <rights license="http://creativecommons.org/licenses/by-sa/3.0/">This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 License</rights>
  </info>
  <locale>
    <terms>
      <term name="editor" form="verb-short">ed.</term>
      <term name="translator" form="verb-short">trans.</term>
      <term name="month-06" form="short">June</term>
      <term name="month-07" form="short">July</term>
      <term name="month-09" form="short">Sept.</term>
      <term name="chapter" form="short">ch.</term>
      <term name="ibid">id.</term>
    </terms>
  </locale>
  <macro name="name-macro">
    <names variable="author">
      <name and="symbol" delimiter=", "/>
      <label form="short" prefix=" "/>
    </names>
  </macro>
  <macro name="author-short">
    <text macro="name-short-macro"/>
  </macro>
  <macro name="name-short-macro">
    <names variable="author">
      <name form="short" and="symbol" delimiter=", "/>
      <label form="verb-short" prefix=", "/>
      <substitute>
        <text variable="title" form="short" font-style="italic"/>
      </substitute>
    </names>
  </macro>
  <macro name="author">
    <choose>
      <if type="legal_case">
        <choose>
          <if variable="author" match="any">
            <group delimiter=": ">
              <text variable="authority"/>
              <group delimiter=" ">
                <text value="Opinion of Advocate General"/>
                <text macro="name-macro"/>
              </group>
            </group>
          </if>
          <else>
            <text variable="authority"/>
          </else>
        </choose>
      </if>
      <else-if type="legislation pamphlet" match="none">
        <text macro="name-macro"/>
      </else-if>
    </choose>
  </macro>
  <macro name="editor-translator">
    <names variable="editor translator" delimiter=", ">
      <name and="symbol" delimiter=", "/>
      <label form="short" strip-periods="true" prefix=" " suffix="."/>
    </names>
  </macro>
  <macro name="title-version">
    <group delimiter=" ">
      <text variable="title"/>
      <text variable="version" prefix="[" suffix="]"/>
    </group>
  </macro>
  <macro name="title-italic-version">
    <group delimiter=" ">
      <text variable="title" font-style="italic"/>
      <text variable="version" prefix="[" suffix="]"/>
    </group>
  </macro>
  <macro name="access">
    <group prefix="(" suffix=")" delimiter=" ">
      <text value="accessed" suffix=" "/>
      <date variable="accessed" delimiter=" ">
        <date-part name="day"/>
        <date-part name="month" form="short"/>
        <date-part name="year"/>
      </date>
    </group>
  </macro>
  <macro name="source">
    <choose>
      <if type="legislation">
        <group delimiter=" ">
          <choose>
            <if variable="jurisdiction">
              <group delimiter=", ">
                <group delimiter=" ">
                  <group delimiter=", ">
                    <group delimiter=": ">
                      <text variable="jurisdiction"/>
                      <text macro="title-version"/>
                    </group>
                    <text variable="number"/>
                    <date variable="original-date">
                      <date-part name="year"/>
                    </date>
                    <text macro="locator"/>
                  </group>
                  <text macro="date-amended"/>
                </group>
                <text macro="container"/>
              </group>
              <date variable="event-date" prefix="(" suffix=")">
                <date-part name="year"/>
              </date>
              <text variable="page-first"/>
            </if>
            <else>
              <group delimiter=", ">
                <text macro="title-version"/>
                <text macro="locator"/>
                <text macro="container"/>
              </group>
              <text macro="year-issued" prefix="(" suffix=")"/>
            </else>
          </choose>
        </group>
      </if>
      <else-if type="pamphlet">
        <group delimiter=", ">
          <group delimiter=" ">
            <text macro="name-macro"/>
            <text macro="title-version"/>
          </group>
          <group delimiter=" ">
            <text variable="genre" font-style="italic"/>
            <text variable="number"/>
          </group>
          <choose>
            <if variable="container-title">
              <group delimiter=" ">
                <text macro="container"/>
                <text variable="page-first"/>
              </group>
              <text macro="locator"/>
            </if>
            <else>
              <text macro="locator"/>
              <text macro="issuance" prefix="(" suffix=")"/>
            </else>
          </choose>
        </group>
      </else-if>
      <else>
        <group delimiter=" ">
          <group delimiter=", ">
            <choose>
              <if type="article-journal article-newspaper article-magazine" match="any">
                <text macro="title-italic-version"/>
                <text macro="container"/>
                <text macro="locator"/>
              </if>
              <else-if type="legal_case">
                <text macro="date-issued"/>
                <choose>
                  <!-- EU case law -->
                  <if variable="jurisdiction">
                    <group delimiter=" ">
                      <text value="Case"/>
                      <text variable="number"/>
                    </group>
                    <text macro="title-italic-version"/>
                    <text macro="container"/>
                  </if>
                  <else-if match="any" variable="title">
                    <text macro="title-italic-version"/>
                  </else-if>
                  <else>
                    <text variable="number"/>
                  </else>
                </choose>
                <text variable="DOI"/>
                <text macro="locator"/>
              </else-if>
              <else-if type="report">
                <group delimiter=" ">
                  <group delimiter=", ">
                    <text macro="title-italic-version"/>
                    <group delimiter=" ">
                      <text variable="genre"/>
                      <text variable="number"/>
                    </group>
                  </group>
                  <text macro="locator"/>
                </group>
              </else-if>
              <else>
                <choose>
                  <if variable="container-title">
                    <text macro="title-italic-version"/>
                    <group delimiter=" ">
                      <text macro="container"/>
                      <text macro="locator"/>
                    </group>
                  </if>
                  <else>
                    <group delimiter=" ">
                      <text macro="title-italic-version"/>
                      <text macro="locator"/>
                    </group>
                  </else>
                </choose>
              </else>
            </choose>
          </group>
          <text macro="issuance" prefix="(" suffix=")"/>
        </group>
      </else>
    </choose>
  </macro>
  <macro name="issuance">
    <choose>
      <if type="article-journal article-magazine article-newspaper broadcast interview manuscript map patent personal_communication song speech thesis webpage" match="any">
        <text macro="date-issued"/>
      </if>
      <else-if type="legal_case" match="none">
        <group delimiter=", ">
          <text macro="editor-translator"/>
          <choose>
            <if match="any" is-numeric="edition">
              <text variable="edition" suffix=" ed."/>
            </if>
            <else>
              <text variable="edition"/>
            </else>
          </choose>
          <group delimiter=" ">
            <text variable="publisher"/>
            <text macro="date-issued"/>
          </group>
        </group>
      </else-if>
    </choose>
  </macro>
  <macro name="online-location">
    <choose>
      <if type="legal_case legislation" match="none">
        <group delimiter=" ">
          <text term="available at"/>
          <choose>
            <if variable="DOI">
              <text variable="DOI"/>
            </if>
            <else>
              <text variable="URL"/>
            </else>
          </choose>
        </group>
      </if>
    </choose>
  </macro>
  <macro name="at_page">
    <group delimiter=" ">
      <text value="at"/>
      <text macro="locator"/>
    </group>
  </macro>
  <macro name="container">
    <group delimiter=" ">
      <choose>
        <if type="chapter paper-conference entry-encyclopedia" match="any">
          <text term="in" font-style="normal"/>
          <text variable="container-title" font-style="italic"/>
        </if>
        <else-if type="article-journal article-newspaper" match="any">
          <text variable="volume"/>
          <text variable="container-title" form="short"/>
          <text variable="issue"/>
        </else-if>
        <else-if type="legal_case legislation">
          <text variable="container-title" form="short"/>
          <text variable="volume"/>
        </else-if>
        <else>
          <text variable="volume"/>
          <text variable="container-title" form="short"/>
        </else>
      </choose>
    </group>
  </macro>
  <macro name="supra-note">
    <group delimiter=" ">
      <text value="supra" font-style="italic"/>
      <text value="n."/>
      <text variable="first-reference-note-number"/>
    </group>
  </macro>
  <macro name="series">
    <text variable="collection-title"/>
  </macro>
  <macro name="date-issued">
    <date delimiter=" " variable="issued">
      <date-part name="day"/>
      <date-part name="month" form="short"/>
      <date-part name="year"/>
    </date>
  </macro>
  <macro name="year-issued">
    <date date-parts="year" form="numeric" variable="issued"/>
  </macro>
  <macro name="date-amended">
    <text variable="references" prefix="(amended " suffix=")"/>
  </macro>
  <macro name="eu-law-year-number">
    <group delimiter="/" prefix="(" suffix=")">
      <text macro="year-issued"/>
      <text variable="number"/>
    </group>
  </macro>
  <macro name="hereinafter">
    <group delimiter=" " prefix="[hereinafter " suffix="]">
      <choose>
        <if variable="title-short" type="book treaty">
          <text variable="title-short" font-style="italic"/>
        </if>
        <else-if type="legislation">
          <choose>
            <if variable="jurisdiction" match="none">
              <text variable="title-short"/>
              <text macro="eu-law-year-number"/>
            </if>
          </choose>
        </else-if>
      </choose>
    </group>
  </macro>
  <macro name="locator">
    <group delimiter=" ">
      <label variable="locator" form="short"/>
      <text variable="locator"/>
    </group>
  </macro>
  <macro name="long-citation">
    <group delimiter=" ">
      <group delimiter=", ">
        <text macro="author"/>
        <text macro="source"/>
        <text macro="series"/>
        <text macro="online-location"/>
      </group>
      <text macro="access"/>
      <text macro="hereinafter"/>
    </group>
  </macro>
  <macro name="short-citation">
    <choose>
      <!-- leading pinpoint locator -->
      <if type="legislation article treaty pamphlet" match="any">
        <group delimiter=" ">
          <text macro="locator"/>
          <choose>
            <if type="legislation">
              <text variable="title-short"/>
              <choose>
                <if variable="jurisdiction">
                  <text macro="year-issued"/>
                  <text macro="date-amended"/>
                </if>
                <else>
                  <text macro="eu-law-year-number"/>
                </else>
              </choose>
            </if>
            <else-if type="pamphlet">
              <text macro="author-short"/>
              <choose>
                <if variable="genre">
                  <text variable="genre"/>
                </if>
                <else>
                  <text variable="title" form="short"/>
                </else>
              </choose>
              <text variable="number"/>
            </else-if>
            <else>
              <text variable="title-short" font-style="italic"/>
              <text macro="year-issued" prefix="(" suffix=")"/>
            </else>
          </choose>
        </group>
      </if>
      <!-- trailing pinpoint locator -->
      <else>
        <group delimiter=", ">
          <choose>
            <if type="legal_case" match="any">
              <group delimiter=" ">
                <choose>
                  <!-- EU case law -->
                  <if variable="jurisdiction">
                    <choose>
                      <if variable="author">
                        <text value="AG Opinion in"/>
                      </if>
                    </choose>
                    <text variable="title" form="short" font-style="italic"/>
                    <text variable="number" prefix="(" suffix=")"/>
                  </if>
                  <else-if match="any" variable="title">
                    <text variable="title" form="short" font-style="italic"/>
                    <text macro="year-issued" prefix="(" suffix=")"/>
                  </else-if>
                  <else>
                    <text variable="number"/>
                    <text macro="date-issued" prefix="(" suffix=")"/>
                  </else>
                </choose>
              </group>
            </if>
            <else>
              <text macro="author-short"/>
              <text macro="supra-note"/>
            </else>
          </choose>
          <text macro="at_page"/>
        </group>
      </else>
    </choose>
  </macro>
  <macro name="sort-types">
    <choose>
      <if type="legislation">
        <text value="1"/>
      </if>
      <else-if type="legal_case">
        <text value="2"/>
      </else-if>
      <else>
        <text value="3"/>
      </else>
    </choose>
  </macro>
  <macro name="sort-jurisdiction">
    <choose>
      <if type="legal_case">
        <text variable="authority"/>
      </if>
      <else-if type="legislation">
        <choose>
          <if variable="jurisdiction">
            <text value="jurisdiction"/>
          </if>
          <else>
            <text value="1"/>
          </else>
        </choose>
      </else-if>
    </choose>
  </macro>
  <macro name="sort-no-author">
    <choose>
      <if variable="author" match="none">
        <text value="1"/>
      </if>
      <else>
        <text value="2"/>
      </else>
    </choose>
  </macro>
  <macro name="sort-author">
    <choose>
      <if type="legislation legal_case" match="none">
        <names variable="author">
          <name form="short" and="symbol" delimiter=", "/>
          <label form="verb-short" prefix=", "/>
        </names>
      </if>
    </choose>
  </macro>
  <macro name="sort-title">
    <group delimiter=" ">
      <choose>
        <if type="legal_case legislation">
          <text variable="number"/>
          <choose>
            <if variable="title">
              <text variable="title"/>
            </if>
            <else>
              <text value="1"/>
            </else>
          </choose>
        </if>
        <else>
          <text variable="title"/>
        </else>
      </choose>
    </group>
  </macro>
  <citation et-al-min="4" et-al-use-first="1">
    <layout suffix="." delimiter="; ">
      <choose>
        <if position="ibid-with-locator">
          <choose>
            <!-- leading pinpoint locator -->
            <if type="legislation article treaty pamphlet" match="any">
              <text macro="short-citation"/>
            </if>
            <else>
              <group delimiter=", ">
                <text term="ibid" font-style="italic"/>
                <text macro="at_page"/>
              </group>
            </else>
          </choose>
        </if>
        <else-if position="ibid">
          <choose>
            <!-- leading pinpoint locator -->
            <if type="legislation article treaty pamphlet" match="any">
              <text macro="short-citation"/>
            </if>
            <else>
              <text term="ibid" font-style="italic"/>
            </else>
          </choose>
        </else-if>
        <else-if position="subsequent">
          <text macro="short-citation"/>
        </else-if>
        <else>
          <text macro="long-citation"/>
        </else>
      </choose>
    </layout>
  </citation>
  <bibliography>
    <sort>
      <key macro="sort-types"/>
      <key macro="sort-jurisdiction"/>
      <key macro="sort-no-author"/>
      <key macro="sort-author"/>
      <key macro="sort-title"/>
      <key variable="issued" sort="descending"/>
    </sort>
    <layout>
      <text macro="long-citation" suffix="."/>
    </layout>
  </bibliography>
</style>

