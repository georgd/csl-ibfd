# CSL Style for IBFD Standard Citations and References — Documentation

## About the Citation Style

This citation style implements the rules as defined in the _Guidelines to the IBFD Standard Citations and References_ (<https://www.ibfd.org/sites/ibfd.org/files/content/pdf/Guidelines-IBFD-Standard-Citations-References.pdf> [hereinafter _Guidelines_]) in its version of 29 June 2017.

Standard CSL and Zotero are not 100% compatible with juridic citation traditions. So this program is an approximation to the rules from the _Guidelines_.

There’s an example database that may serve as a reference at <https://www.zotero.org/groups/2507501/ibfd_examples>.

The style variant “IBFD Standard Citations and References (with page label)” differs from the original style file in the addition of the label “p.” for page locators. This off-guideline use seems to appear frequently in IBFD articles.

## Sorting

CSL styles don’t allow a subdivided bibliography but it’s possible to sort the references by entry types. This style lists laws at the top, followed by case law and all other entries at last. After finishing work, the citations can be unlinked and the subdivisions finalized. 

## Citing

This style implements footnote citations as required by the _Guidelines_. It does not support textual references as CSL allows a style to exclusively define either notes or in-text-refences.

Pinpoint references are printed with an abbreviated label exept if ‘page’ is chosen, then no label will be printed. Pinpoint references for which no choice exists in the Zotero citation dialogue can be entered using the ‘page’ option, including the required label. 


## Data Entry

### General

#### Using the Extra Field

The nature of the style requires the user to fill in some information into Zotero fields designated with captions suggesting otherwise. These cases are marked in the following sections. Also, the Zotero user interface doesn’t show all possible fields. These can be added to the _Extra_ field (s. <https://www.zotero.org/support/kb/item_types_and_fields#citing_fields_from_extra>):

Enter each variable on a separate line at the top of the _Extra_ field in the following format:

```
CSL Variable: Value
```

For example:

```
collection-title: Models IBFD
genre: OECD Model
```

#### The IBFD Collection Name

The collection name is saved in the variable ‘collection-title’. In most document types this has to be entered in the Extra field (s. above). In some however, Zotero shows the variable in the user interface—but under varying field names. In Book it’s called ‘Series’, in Report it’s ‘Series title’.

#### Translated titles

According to the _Guidelines_, translated titles or translation remarks (like ‘unofficial translation’) are put in brackets after the title. This is realized using the variable ‘version’. This always has to be put in the _Extra_ field.

### Book

Most materials listed in the _Guidelines_ are entered as **Book**: 

- News
- Quick reference tables
- Country key features
- Country surveys
- Country analyses
- Topical analyses
- Global tax treaty commentaries
- International organizations’ documentation
- Books
- Edited books

| Content                   | Field                  |
| ---------                 | -------                |
| Surname, Initials author  | Author (last), (first) |
| Title: Subtitle           | Title                  |
| Surname, Initials editor  | Editor (last), (first) |
| Edition                   | Edition                |
| Publisher                 | Publisher              |
| Year                      | Date                   |
| Collection name           | Series                 |
| Short title (hereinafter) | Short Title            |
| Accessed date*            | Accessed               |

\* The accessed date is only printed if no other date is present. The _Guidelines_ aren’t explicit on this but it never occurs together with another date.

### Book section

[In the _Guidelines_ as ‘book containing articles by multiple authors’]

| Content                   | Field                  |
| ---------                 | -------                |
| Surname, Initials author  | Author (last), (first) |
| Title: Subtitle           | Title                  |
| Book title                | Book Title             |
| Surname, Initials editor  | Editor (last), (first) |
| Edition                   | Edition                |
| Publisher                 | Publisher              |
| Year                      | Date                   |
| Collection name           | Series                 |
| Short title (hereinafter) | Short Title            |
| Accessed date             | Accessed               |

### Glossary

Entry type: **Encyclopedia Article**

| Content                   | Field                  |
| ---------                 | -------                |
| Term                      | Title                  |
| Title                     | Encyclopedia Title     |
| Surname, Initials editor  | Editor (last), (first) |
| Edition                   | Edition                |
| Publisher                 | Publisher              |
| Year                      | Date                   |
| Collection name           | Series                 |
| Short title (hereinafter) | Short Title            |
| Accessed date             | Accessed               |

### Journal Article

| Content                   | Field                  |
| ---------                 | -------                |
| Surname, Initials author  | Author (last), (first) |
| Title: Subtitle           | Title                  |
| Volume number             | Volume                 |
| Abbrev. periodical title  | Publication            |
| Issue number              | Issue                  |
| Year                      | Date                   |
| Collection name           | Series                 |
| Short title (hereinafter) | Short Title            |
| Accessed date             | Accessed               |

### Treaties

There is a *treaty* type in CSL but no such type in the Zotero user interface. Any type can be used and will be overridden in the *Extra* field. (The Examples database uses the **Document** item type).

| Content                         | Field                                    |
| ---------                       | -------                                  |
| Title                           | Title                                    |
| [unofficial translation]        | Extra: version: [unofficial translation] |
| Date of signature               | Date                                     |
| Collection name                 | Extra: collectin-title: Treaties IBFD    |
| Abbreviated title (hereinafter) | Short Title                              |

The *Extra* field should look like this:

```
type: treaty
version: unofficial translation
collection-title: Treaties IBFD
```

### Models

Use the item type **Document**

| Content                         | Field                                |
| ---------                       | -------                              |
| Title                           | Title                                |
| Date                            | Date                                 |
| Collection name                 | Extra: collection-title: Models IBFD |
| Abbreviated title (hereinafter) | Short Title                          |


An example *Extra* field:

```
collection-title: Models IBFD
```

### Legislation

- Use the item type **Statute**.
- The citation processor formats citations without an entry in the _jurisdiction_ field as EU law.

#### EU Law

| Content                                               | Field                                |
| ---------                                             | -------                              |
| Full Title                                            | Title                                |
| Source title [OJ]                                     | Code                                 |
| Issue no. of the OJ where the directive was published | Code number                          |
| Year (included in hereinafter)                        | Date enacted                         |
| Collection name                                       | Extra: collection-title: EU Law IBFD |
| Abbreviated title (hereinafter title)                 | Short Title                          |
| Number of the directive (included in hereinafter)     | Public Law Number                    |



#### National Legislation

| Content                                       | Field                                    |
| ---------                                     | -------                                  |
| Abbreviated country name                      | Extra: jurisdiction: CN                  |
| Title law in full                             | Title                                    |
| Translation in English                        | Extra: version: Translation in English   |
| Type of law and/or number w/ date and/or year | Public Law Number                        |
| Year of law                                   | Date enacted                             |
| Amended year                                  | History                                  |
| Abbreviated Source                            | Code                                     |
| Volume number                                 | Code number                              |
| Source year                                   | Extra: event-date: YYYY                  |
| Start page number                             | Pages                                    |
| Collection name                               | Extra: collection-title: Collection name |
| Abbreviated title (hereinafter)               | Short Title                              |

Additionally, if the year is not contained in the title or the type and number of law, it has to be entered in Extra: original-date: YYYY

### Tax Authorities’ Documentation

This style uses the _pamphlet_ type for Tax Authorities’s Documentation. Zotero doesn’t show it in the interface, so, like with treaties, any type can be used and you have to override it by an entry in the _Extra_ field (The Examples database uses the **Report** item type which provides all necessary fields).

| Content                                | Field                                |
| ---------                              | -------                              |
| Abbreviation of the tax administration | Author (full name)                   |
| Pronouncement                          | Title                                |
| Documentation type                     | Report Type                          |
| Year-number                            | Report Number                        |
| Date                                   | Date                                 |
| Source title                           | Extra: container-title: Source title |
| Source volume                          | Extra: volume: Source volume         |
| Start page number                      | Pages                                |
| Collection name                        | Series Title                         |

An example *Extra* field:

```
type: pamphlet
```


### Case Law

- Use the item type **Case**.
- The citation processor formats citations with an entry in the _jurisdiction_ field as EU case law.

#### Tax Treaty Case Law

| Content                                  | Field                                    |
| ---------                                | -------                                  |
| Abbrev. country name: abbrev. court name | Court                                    |
| Year or date of decision                 | Date Decided                             |
| Case name                                | Title                                    |
| Case number                              | Docket Number                            |
| Source title                             | Reporter                                 |
| Source number                            | Reporter Volume                          |
| Collection name                          | Extra: collection-title: Collection name |
| Abbreviated title (hereinafter)          | Short Title                              |
| ECLI                                     | Extra: DOI: ...                          |

Don’t use the jurisdiction field!

#### ECJ Case Law

Like Tax Treaty Case Law but needs an entry in _Extra: jurisdiction_

| Content                                  | Field                                    |
| ---------                                | -------                                  |
| Abbrev. country name: abbrev. court name | Court                                    |
| Year or date of decision                 | Date Decided                             |
| Case name                                | Title                                    |
| Case number                              | Docket Number                            |
| Source title                             | Reporter                                 |
| Source number                            | Reporter Volume                          |
| Collection name                          | Extra: collection-title: Collection name |
| Abbreviated title (hereinafter)          | Short Title                              |
| ECLI                                     | Extra: DOI: ...                          |
| Entry in jurisdiction field              | Extra: jurisdiction: EU                  |

#### AG Opinion

| Content                         | Field                                    |
| ---------                       | -------                                  |
| Abbrev. country name            | Court                                    |
| Advocate General’s surname      | Author (last)                            |
| Date of Opinion                 | Date Decided                             |
| Case name                       | Title                                    |
| Case number                     | Docket Number                            |
| Source title                    | Reporter                                 |
| Source number                   | Reporter Volume                          |
| Collection name                 | Extra: collection-title: Collection name |
| Abbreviated title (hereinafter) | Short Title                              |
| ECLI                            | Extra: DOI: ...                          |
| Entry in jurisdiction field     | Extra: jurisdiction: EU                  |




